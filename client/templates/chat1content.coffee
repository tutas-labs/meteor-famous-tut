
Template.chat1content.posting = ->
    query = Chat.find {}
    handle = query.observe
        added: (doc)->
            $('#pbody').scrollTop($('#pbody')[0].scrollHeight - $('#pbody')[0].clientHeight);
    return query

Template.chat1content.events
      'click #ibtn':  (evt,tmpl) ->
        t = this._id
        Meteor.call 'removeChat',t

      'click #bbtn': (evt,tmpl) ->
        Router.go('/')

      'click #ebtn': (evt,tmpl) ->
        d = tmpl.find("#tfield").value
        Meteor.call 'newChat',{text: d}, (error,res) ->
          if error
            alert error.reason.sanitizedError.reason
          $("#tfield").val("")
