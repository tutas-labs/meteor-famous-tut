Meteor.startup  ->
#generic simple scroll page of one surface
  class App.Drag extends Famous.View
    DEFAULT_OPTIONS:
      content: undefined
    constructor: (@options) ->
      super @options
      @createPage()
    createPage: ->
      #create a node of a view with scroll added
      #major hack here
      position = new Famous.Transitionable [0,0]
      sync = new Famous.MouseSync()

      surface = new Famous.Surface
        size : [200,200]
        content: 'Drag Me'
        properties :
          background : 'red'
          color: 'white'
          textAlign: 'center'
          lineHeight: '200px'
          fontSize: 'x-large'
      surface.pipe(sync);
      sync.on 'update', (data) =>
        currentPosition = position.get()
        position.set [currentPosition[0] + data.delta[0],currentPosition[1] + data.delta[1]]
      sync.on 'end', =>
# transition the position back to [0,0] with a bounce
        position.set [0,0], {curve : 'easeOutBounce', duration : 300}


      positionModifier = new Famous.Modifier
        transform : ->
          currentPosition = position.get();
          return Famous.Transform.translate(currentPosition[0], currentPosition[1], 0)
      centerModifier = new Famous.Modifier
        origin : [0.5, 0.5]
      b2 = new Famous.Surface
        size: [80, 40]
        content: 'Home'
        classes: ['btn','btn-primary']
        properties:
          textAlign: 'center'
          fontSize: 'large'
      b2.on 'click', =>
        Router.go '/'

      @add(new Famous.Modifier({origin: [0.5,0.9]})).add b2
      @add(centerModifier).add(positionModifier).add(surface)