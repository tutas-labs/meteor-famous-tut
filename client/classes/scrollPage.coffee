Meteor.startup  ->
#generic simple scroll page of one surface
  class App.ScrollPage extends Famous.View
    DEFAULT_OPTIONS:
      content: undefined
    constructor: (@options) ->
      super @options
      @createPage()
    createPage: ->
      @contentView = new Famous.Scrollview
      #create a node of a view with scroll added
      @mainNode = @add @contentView
#major hack here
      div = document.createElement('div')
      Blaze.render @options.content,div
      @contents = new Famous.Surface
        size: [undefined, undefined ]
        content: div

      @contents.pipe @contentView

      @contentView.sequenceFrom [@contents]

      @contents.on 'deploy', =>
        h = $('.famous-container')[0].scrollHeight
        @contents.setSize [undefined, h]
