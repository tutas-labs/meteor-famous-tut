Meteor.startup  ->
#generic simple scroll page of one surface
  class App.ScrollItems extends Famous.View
    DEFAULT_OPTIONS:
      content: undefined
    constructor: (@options) ->
      super @options
      @createPage()
    createPage: ->
      @layout = new Famous.HeaderFooterLayout
        headerSize: @options.headsize
        footerSize: @options.footsize
      @contentView = new Famous.Scrollview

      @footer = new Famous.View
      @head = new Famous.View

      @footbacking = new Famous.Surface
        classes: ['footer']

      @headerbacking = new Famous.Surface
        classes: ['header']


      @footer.add(new Famous.StateModifier({transform: Famous.Transform.translate(0,0,10)})).add @footbacking
      @head.add(new Famous.StateModifier({transform: Famous.Transform.translate(0,0,10)})).add @headerbacking



      #create a node of a view with scroll added
      @layout.content.add @contentView
      @layout.footer.add @footer
      @layout.header.add @head

      @mainNode = @add @layout

