Meteor.startup  ->
#generic simple scroll page of one surface
  class App.Box extends Famous.View
    DEFAULT_OPTIONS:
      content: undefined
    constructor: (@options) ->
      super @options
      @createPage()
    createPage: ->
      #create a node of a view with scroll added
      #major hack here

      @contents = new Famous.Surface
        size: [200, 200 ]
        content: 'This is a green box'
        classes: ['backing']
        properties:
          textAlign: 'center'
          fontSize: 'large'
          lineHeight: '200px'
          color: 'white'

      b2 = new Famous.Surface
        size: [80, 40]
        content: 'Home'
        classes: ['btn','btn-primary']
        properties:
          textAlign: 'center'
          fontSize: 'large'
      @add(new Famous.Modifier({origin: [0.5,0.5]})).add @contents
      @add(new Famous.Modifier({origin: [0.5,0.9]})).add b2
      @contents.on 'click', =>
        alert 'box clicked'
      b2.on 'click', =>
        Router.go '/'

