Router.map ->
  this.route 'home',
    path: '/'
  this.route 'about',
    path: '/about'
  this.route 'box',
    path: '/box'
  this.route 'drag',
    path: '/drag'
  this.route 'chat1',
    path: '/chat1'
    waitOn: ->
      return Meteor.subscribe 'chat'
  this.route 'chat2',
    path: '/chat2'
    waitOn: ->
      return Meteor.subscribe 'chat'



Router.onRun ->
  $('.famous-container').remove()
