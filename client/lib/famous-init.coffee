
Meteor.startup ->

  require "famous-polyfills"


  require "famous/core/famous"
  Famous.Engine = require 'famous/core/Engine'
  Famous.View = require 'famous/core/View' 
  Famous.Deck = require 'famous/views/Deck' 
  Famous.Surface = require 'famous/core/Surface' 
  Famous.Modifier = require 'famous/core/Modifier' 
  Famous.Transform = require 'famous/core/Transform' 
  Famous.Draggable = require 'famous/modifiers/Draggable' 
  Famous.StateModifier = require 'famous/modifiers/StateModifier' 
  Famous.RenderController = require "famous/views/RenderController" 
  Famous.EventHandler = require "famous/core/EventHandler"
  Famous.ViewSequence = require "famous/core/ViewSequence"

  Famous.Scrollview = require 'famous/views/Scrollview' 
  Famous.HeaderFooterLayout = require "famous/views/HeaderFooterLayout" 

  Famous.Easing = require 'famous/transitions/Easing' 
  Famous.Transitionable = require 'famous/transitions/Transitionable' 
  Famous.SnapTransition = require 'famous/transitions/SnapTransition' 
  Famous.SpringTransition = require 'famous/transitions/SpringTransition' 
  Famous.GenericSync     = require 'famous/inputs/GenericSync' 
  Famous.MouseSync       = require 'famous/inputs/MouseSync' 
  Famous.TouchSync       = require 'famous/inputs/TouchSync' 
  Famous.Timer           = require 'famous/utilities/Timer' 
  Famous.InputSurface    = require 'famous/surfaces/InputSurface' 
  Famous.FastClick       = require 'famous/inputs/FastClick' 

#Initialize two basic transitions.
  Famous.Transitionable.registerMethod 'snap', Famous.SnapTransition 
  Famous.Transitionable.registerMethod 'spring', Famous.SpringTransition 

