Template.chat1.rendered = ->

  App.mainContext = Famous.Engine.createContext()

  appViews = new App.ScrollPage content: Template.chat1content
  appViews.contents.unpipe appViews.contentView
  st = new Famous.StateModifier
    transform: Famous.Transform.rotateX(Math.PI / 2)
  appViews.show = (callback) =>
    st.setTransform(Famous.Transform.rotateX(0),{duration: 500,curve: 'easeOut'},callback)
  App.mainContext.add(st).add appViews
  appViews.show()
