
Template.chat2.rendered = ->

  App.mainContext = Famous.Engine.createContext()
  headsize = 125
  App.appHeadFoot = new App.ScrollItems {content: '',headsize: headsize,footsize: 50}

  App.viewSequence = new Famous.ViewSequence
  App.appHeadFoot.contentView.sequenceFrom App.viewSequence


  createFooter =  ->
    tempView = new Famous.View
    originModHome = new Famous.StateModifier
      origin: [0.03,0.5]
      transform: Famous.Transform.translate(0,0,20)
    buthome = new Famous.Surface
      size: [75, 45]
      content: "Home"
      classes: ['btn','btn-primary','mybtn']
      properties:
        lineHeight: '40px'
        textAlign: 'center'
    buthome.on 'click', =>
      handle.stop()
      Router.go '/'
    node = tempView.add(originModHome).add buthome
    return tempView

  createHeader =  ->
    headText = new Famous.Surface
      size: [undefined , 45]
      content: "Meteor and Famo.us Let's Chat!"
      classes: ['headText']
      properties:
        lineHeight: '45px'
        textAlign: 'center'
    originModAdd = new Famous.StateModifier
      origin: [0.03,0.7]
      transform: Famous.Transform.translate(0,0,20)
    sizeModInput = new Famous.Modifier
    sizeModInput.sizeFrom ->
      size = App.mainContext.getSize()
      return [0.6 * size[0],0.4 * headsize]
    originModInput = new Famous.StateModifier
      origin: [0.55,0.7]
      transform: Famous.Transform.translate(0,0,20)
    butadd = new Famous.Surface
      size: [75, 45]
      content: "Add"
      classes: ['btn','btn-primary','mybtn']
      properties:
        lineHeight: '40px'
        textAlign: 'center'
    App.inputf = new Famous.InputSurface
      classes: ['inputf']
    butadd.on 'click', =>
      xx =App.inputf.getValue()
      Meteor.call 'newChat',{text: xx}, (error,res) ->
        if error
          alert error
      App.inputf.setValue ""
    App.inputf.setPlaceholder "Enter note"
    App.appHeadFoot.head.add(new Famous.StateModifier({origin: [0,0],transform: new Famous.Transform.translate(0,0,20)})).add headText
    App.appHeadFoot.head.add(originModAdd).add butadd
    App.appHeadFoot.head.add(originModInput).add(sizeModInput).add App.inputf
    return true

  torf = createHeader()
  thv = createFooter()

  App.appHeadFoot.footer.add thv
  App.mainContext.add App.appHeadFoot

  query = Chat.find({})
  handle = query.observe
    addedAt: (doc,ind,bef)=>

        if ind/2 - Math.floor(ind/2) > 0 then doc.evenodd = "todd" else doc.evenodd = "teven"
        mdate = moment doc.createdAt
        doc.dhuman = mdate.fromNow()
        tm = Blaze.toHTMLWithData(Template.chat2content,doc)
        node = createSurface tm,doc._id

        App.viewSequence.splice ind,0,node
        sz = App.appHeadFoot.contentView.getSize()
        inx = sz[1]/75

        if ind > inx
          App.appHeadFoot.contentView.goToNextPage()
    removedAt: (doc,ind) ->
        App.viewSequence.splice(ind,1)

    changedAt: (newdoc,olddoc,ind) =>
        if ind/2 - Math.floor(ind/2) > 0 then newdoc.evenodd = "todd" else newdoc.evenodd = "teven"
        mdate = moment newdoc.createdAt
        newdoc.dhuman = mdate.fromNow()
        tm = Blaze.toHTMLWithData(Template.chat2content,newdoc)
        node = createSurface tm,newdoc._id
        App.viewSequence.splice(ind,1)
        App.viewSequence.splice ind,0,node




  createSurface = (content,idx) ->
      tempView = new Famous.View

      sizeMod = new Famous.StateModifier
        size: [undefined, 75]
      node = tempView.add(sizeMod)
      surface = new Famous.Surface
        content: content
        size: [undefined, undefined ]
        classes: ['secondary-bg']
        properties:
          lineHeight: '25px'
          textAlign: 'left'
      but = new Famous.Surface
        size: [50,50]
        content: 'Del'
        classes: ['btn','btn-primary','id'+idx]
        properties:
          lineHeight: '35px'
          textAlign: 'center'
      but.on 'click' ,(data)=>
        cl = data.target.className
        tp = cl.slice(33)
        Meteor.call('removeChat',tp)

      node.add surface
      node.add(new Famous.StateModifier({origin: [1,.5],transform: Famous.Transform.translate(0,0,5)})).add but

      surface.pipe App.appHeadFoot.contentView
      return tempView




