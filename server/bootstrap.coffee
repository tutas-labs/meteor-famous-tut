
Meteor.startup  ->
  if Chat.find().count() is 0
    Chat.insert text: 'this is a test'


Meteor.methods
  newChat:  (post) ->
    Chat.insert post,
      (error,res) =>
        if error
          throw new Meteor.Error(404, error)
          return error
        else
          return ''
  removeChat: (id) ->
    Chat.remove _id: id,
      (error,res) =>
        if error
          throw new Meteor.Error(404, error)
          return error
        else
          return ''